Leo Huang

This program takes a list of atomic statements and a propositional formula.  
It evaluates the formula and all sub-formulas in every possible truth assignment, then constructs a truth table.  

Run on the command line with 'python propeval.py'  

Previous version  
It used to take a truth assignment from the user and then evaluate the formula according to it.  
The current implementation is more useful as it covers every case and no longer requires the user know the atomic values beforehand.