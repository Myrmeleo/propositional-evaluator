import enum


class Connective(enum.Enum):
    Not = 1
    Or = 2
    And = 3
    Implies = 4
    Iff = 5

""" 
A statement is one of the following:
    An atom
    An n-ary logical connective connecting n statements (call this a molecular statement)
"""
class Statement:
    def __init__(self):
        pass
    #returns Boolean
    def evaluate(self):
        pass
    #returns String
    def to_string(self):
        pass
    #modify truth value if the name specified matches this name
    #param: String Boolean
    def setAtom(self, name, newVal):
        pass

class Atom(Statement):
    #params: Atom, String, Boolean
    def __init__(self, name, value=False):
        super().__init__()
        self.name = name
        self.value = value

    #returns Boolean
    def evaluate(self):
        return self.value

    #returns String
    def to_string(self):
        return self.name

    #modify truth value if the name specified matches this name
    #param: String Boolean
    def setAtom(self, name, newVal):
        if name == self.name:
            self.value = newVal

    #we must define equals in order for set() to correctly recognize and remove duplicates from a list of atoms
    def __eq__(self, other):
        return isinstance(other, Atom) and self.name == other.name

    #if we define equals, we might as well define not-equals
    def __ne__(self, other):
        return (not isinstance(other, Atom)) or self.name != other.name

    """
    In order for Atom to be hashable, we must define hash
    Each Atom has a name that never changes, therefore they are guaranteed to have a hash value that never changes
    """
    def __hash__(self):
        return hash(self.name)


class Molecule(Statement):
    """
    params: Connective, Statement[]
        args must be an appropriate size for the connective (ex. 2 args for a binary connective)
    """
    def __init__(self, connective, args=None):
        super().__init__()
        self.connective = connective
        if args is not None: self.args = args
        else: self.args = None
        #get a list of all the atoms in this molecule
        atoms = []
        #also get a list of all submolecules (molecules on either side of the connective) in the molecule
        subMols = []
        for subForm in args:
            #the atoms and submolecules of this includes those of the subformulas
            if isinstance(subForm, Atom):
                atoms.append(subForm)
            elif isinstance(subForm, Molecule):
                atoms.extend(subForm.atoms)
                subMols.extend(subForm.subMols)
                subMols.append(subForm)
        #remove duplicates
        self.atoms = sorted(set(atoms), key=lambda x: x.to_string())
        self.subMols = sorted(set(subMols), key=lambda x: len(x.to_string()))

    #returns Boolean
    def evaluate(self):
        if self.connective == Connective.Not:
            if self.args[0].evaluate() == False:
                return True
            elif self.args[0].evaluate() == True:
                return False
        elif self.connective == Connective.Or:
            if self.args[0].evaluate() == True or self.args[1].evaluate() == True:
                return True
            elif self.args[0].evaluate() == False and self.args[1].evaluate() == False:
                return False
        elif self.connective == Connective.And:
            if self.args[0].evaluate() == True and self.args[1].evaluate() == True:
                return True
            elif self.args[0].evaluate() == False or self.args[1].evaluate() == False:
                return False
        elif self.connective == Connective.Implies:
            if self.args[0].evaluate() == False or self.args[1].evaluate() == True:
                return True
            elif self.args[0].evaluate() == True and self.args[1].evaluate() == False:
                return False
        elif self.connective == Connective.Iff:
            if (self.args[0].evaluate() == True and self.args[1].evaluate() == True) or (self.args[0].evaluate() == False and self.args[1].evaluate() == False):
                return True
            elif (self.args[0].evaluate() == True and self.args[1].evaluate() == False) or (self.args[0].evaluate() == False and self.args[1].evaluate() == True):
                return False

    #return String
    def to_string(self):
        if self.connective == Connective.Not:
            return "!" + self.args[0].to_string()
        elif self.connective == Connective.Or:
            return "(" + self.args[0].to_string() + " | " + self.args[1].to_string() + ")"
        elif self.connective == Connective.And:
            return "(" + self.args[0].to_string() + " & " + self.args[1].to_string() + ")"
        elif self.connective == Connective.Implies:
            return "(" + self.args[0].to_string() + " -> " + self.args[1].to_string() + ")"
        elif self.connective == Connective.Iff:
            return "(" + self.args[0].to_string() + " <-> " + self.args[1].to_string() + ")"

    """
    change the value of the atom with given name to the given new value
    that means go through all occurrences of atom and change them
    param: String Boolean
    """
    def setAtom(self, name, newVal):
        for subForm in self.args:
            subForm.setAtom(name, newVal)

    def __eq__(self, other):
        return isinstance(other, Molecule) and self.to_string() == other.to_string()

    def __ne__(self, other):
        return (not isinstance(other, Molecule)) or self.to_string() != other.to_string()

    def __hash__(self):
        return hash(self.to_string())


"""
Prints the truth table for the given statement
param: Statement
effect: prints a truth table to the console
"""
def print_evaluation(statement):
    #get a list of the atoms in our statement
    if isinstance(statement, Atom):
        atoms = [statement]
        subMols = []
    elif isinstance(statement, Molecule):
        atoms = statement.atoms
        subMols = statement.subMols
    #first, the leading text and table header
    print("Your statement has the following truth table:")
    header = ""
    for atom in atoms:
        header += "|{:^{padding}}".format(atom.to_string(), padding=6)
    for s in subMols:
        header += "|{:^{padding}}".format(s.to_string(), padding=len(s.to_string()) + 4)
    header += "|{:^{padding}}".format(statement.to_string(), padding=len(statement.to_string()) + 4)
    print(header)
    """
    now show the resulting truth values for every combination of atomic values (2^n rows total, where n is the atom count)
    the assignment of atomic truth values for each row follows this convention:
        Let the atoms be numbered 1, 2, and so on going from left to right.
        For the atom numbered k, assign it a truth value of 1 for the first 2^(n - k) rows, 
            then a value of 0 for the next 2^(n - k) rows. Repeat until all rows are assigned.
    """
    numRows = pow(2, len(atoms))
    for i in range(0, numRows):
        currentLine = ""
        for k in range(1, len(atoms) + 1):
            #we take advantage of the fact that int//int division returns a floored int
            #assign a truth value of 1 for the first 2^(n - k) rows
            if i // pow(2, len(atoms) - k) % 2 == 0:
                newAtomVal = True
            #assign a truth value of 0 for the next 2^(n - k) rows
            else:
                newAtomVal = False
            atoms[k-1].value = newAtomVal
            statement.setAtom(atoms[k-1].name, newAtomVal)
            currentLine += "|{:^{padding}}".format(atoms[k-1].value, padding=6)
        #evaluate the subformulas and the total formula using the truth assignment
        for s in subMols:
            currentLine += "|{:^{padding}}".format(s.evaluate(), padding=len(s.to_string()) + 4)
        currentLine += "|{:^{padding}}".format(statement.evaluate(), padding=len(statement.to_string()) + 4)
        print(currentLine)