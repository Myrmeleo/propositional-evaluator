#This file is never actually used in the program; I had the idea but scrapped it

"""
A transition rule is a 3-tuple (start input end)
    start is the initial state
    input is the input required for the transition
    end is the resulting state
"""
class TransitionRule:
    #params: String String String
    def __init__(self, start, input_, end):
        self.start = start
        self.input_ = input_
        self.end = end


"""
A deterministic finite automaton (DFA) consists of the following
    A set of states
    A set of inputs (in this case, each input is a one-character string)
        In this case, "set of inputs" means all inputs for which the state transitions, not the set of all possible inputs the user could enter
    A set of transition rules
    A start state, which is a member of the set of states
    A set of acceptable states, all of which are in the set fo states
        If the DFA does not end on one of these states. reject the input string, otherwise accept the input string
"""
class DFA:
    """
    params: [Strings] [Strings] [TransitionRules] [String] [Strings]
        the inputs in the transition rules must be members of inputs
    """
    def __init__(self, states, inputs, transitions, start, acceptables):
        self.states = states
        self.inputs = inputs
        self.transitions = transitions
        self.start = start
        self.acceptables = acceptables
