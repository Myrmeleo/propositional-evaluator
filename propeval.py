import enum
from propositionalstatement import *


#actions we prompt the user to do
class Tasks(enum.Enum):
    EnterAtoms = 1
    EnterFormula = 2


"""
take expression and generate a list of tokens, parsing one character at a time
a token is one of {(, ), !, |, &, ->, <->, [Atom]}
param: String
returns [Strings]
effects: prints an error message and returns None if expression is invalid
"""
def generate_tokens(expression):
    tokens = []
    #token will need to be built one character at a time
    current_token = ""
    #function that adds the token to the list and resets it
    """
    add the current token to the list of tokens to be returned
    returns: Boolean
        whether the token was a valid one
    side effect: adds to token list
    """
    def add_current_token():
        nonlocal current_token
        if len(current_token) > 0:
            #translate alphabetic tokens to their symbolic counterpart
            if current_token == "not":
                tokens.append("!")
            elif current_token == "or":
                tokens.append("|")
            elif current_token == "and":
                tokens.append("&")
            elif current_token == "implies":
                tokens.append("->")
            elif current_token == "iff":
                tokens.append("<->")
            #add symbol tokens
            elif current_token == "(" or current_token == ")" or current_token == "!" or current_token == "|" or current_token == "&" or current_token == "->" or current_token == "<->":
                tokens.append(current_token)
            #add any atomic token
            elif len(current_token) == 1 and current_token.isalpha():
                tokens.append(current_token)
            else:
                print("Error: Invalid token: {0}".format(current_token))
                return False
        current_token = ""
        return True
    current_index = 0
    #check for empty expression
    if len(expression) == 0:
        print("Error: Empty statement given.")
        return None
    while current_index < len(expression):
        next_char = expression[current_index: current_index + 1]
        #space means that the current token being built is complete
        if next_char == " ":
            if not add_current_token():
                return None
        #any of these characters means the beginning of a new token, so the current token is finished
        elif next_char == "(" or next_char == ")" or next_char == "|" or next_char == "&" or next_char == "<" or next_char == "!":
            if not add_current_token():
                return None
            current_token += next_char
        #this character is part of a connective, but - could be the start of one
        elif next_char == "-" or next_char == ">":
            #check if this character comes after an alphabetic string, in which case it's the start of a new token
            if current_token.isalpha():
                if not add_current_token():
                    return None
            current_token += next_char
        #a letter character could mean the start of a new token if the current token is not alphabetic
        elif next_char.isalpha():
            if not current_token.isalpha():
                if not add_current_token():
                    return None
            current_token += next_char
        else:
            current_token += next_char
        current_index += 1
    if current_token != "":
        if not add_current_token():
            return None
    return tokens


"""
construct a Statement that models the propositional statement given by the formula
use the atoms with their assigned values to build the statement
params: [Strings] [Strings]
   each token is one of {(, ), !, |, &, ->, <->, (atom)}
returns: Statement or None
   the statement we constructed
   None if the formula was not a well-formed formula
effects: prints an error message and returns None if formula is not well-formed
"""
def parse_formula(atoms, tokens):
    #list of atoms that are not in the parameter
    foreign_atoms = list(filter(lambda a: a.isalpha() and len(a) == 1 and a not in atoms, tokens))
    #check for atoms that are not in the given list
    if len(foreign_atoms) > 0:
        print("Error: Unrecognized atoms: " + ", ".join(foreign_atoms) + ". Your atoms are " + ", ".join(atoms))
        return None
    #check for unequal numbers of left and right brackets
    elif tokens.count("(") != tokens.count(")"):
        print("Error: Unequal numbers of left and right parentheses. {0} compared to {1}".format(tokens.count("("), tokens.count(")")))
        return None
    else:
        """
        The first symbol in a well-formed formula will always be one of the following
            The word 'not' or the symbol !
            An atom
            A left bracket
        """
        current_token = tokens[0]
        #check for the 'not' connective
        if current_token == "!":
            arg1 = parse_formula(atoms, tokens[1:])
            return None if arg1 is None else Molecule(Connective.Not, [arg1])
        #check for an atom
        elif current_token.isalpha() and len(current_token) == 1:
            """
            2 possible cases:
                1: this atom is the entire statement
                2: this atom is the left side of a binary connective
            check case 1
            """
            index_in_atoms = atoms.index(current_token)
            if len(tokens) == 1:
                return Atom(current_token)
            #check case 2
            else:
                left_side = Atom(current_token)
                connective = tokens[1]
                #make sure there is a right side to the connective
                if len(tokens) < 3:
                    print("Error: Nothing on right side of {0} {1}".format(left_side.to_string(), connective))
                    return None
                #if the right side has surrounding brackets, they are not removed before being passed in
                right_side = parse_formula(atoms, tokens[2:])
                if not right_side is None:
                    #build statement depending on the connective
                    if connective == "|":
                        return Molecule(Connective.Or, [left_side, right_side])
                    elif connective == "&":
                        return Molecule(Connective.And, [left_side, right_side])
                    elif connective == "->":
                        return Molecule(Connective.Implies, [left_side, right_side])
                    elif connective == "<->":
                        return Molecule(Connective.Iff, [left_side, right_side])
                    else:
                        print("Error: Illegal symbol " + connective + " between " + tokens[0] + " and " + " ".join(tokens[2:]))
                        return None
        #check for a left parenthesis
        elif current_token == "(":
            """
            2 possible cases:
               1: this set of brackets encloses the entire statement
               2: this set of brackets encloses the left side of a binary connective
            We will find the right bracket that matches this one.
            If it is the last element in the formula, then we have case 1. Otherwise we have case 2
            We have found our right bracket when the number of right brackets equals the number of lefts
            """
            left_brackets = 1
            right_brackets = 0
            right_location = 1
            while right_location < len(tokens):
                if tokens[right_location] == "(":
                    left_brackets += 1
                elif tokens[right_location] == ")":
                    right_brackets += 1
                if right_brackets == left_brackets:
                    break
                right_location += 1
            #check case 1
            if right_location == len(tokens) - 1:
                #check the statement contained in the brackets
                return parse_formula(atoms, tokens[1: right_location])
            #check case 2
            else:
                #the left side already has its surrounding brackets removed before being passed in
                left_side = parse_formula(atoms, tokens[1: right_location])
                connective = tokens[right_location + 1]
                #make sure there is a right side to the connective
                if len(tokens) < right_location + 3:
                    print("Error: Nothing on right side of {0} {1}".format(left_side.to_string(), connective))
                    return None
                #if the right side has surrounding brackets, they are not removed before being passed in
                right_side = parse_formula(atoms, tokens[right_location + 2:])
                if (not left_side is None) and (not right_side is None):
                    #build statement depending on the connective
                    if connective == "|":
                        return Molecule(Connective.Or, [left_side, right_side])
                    elif connective == "&":
                        return Molecule(Connective.And, [left_side, right_side])
                    elif connective == "->":
                        return Molecule(Connective.Implies, [left_side, right_side])
                    elif connective == "<->":
                        return Molecule(Connective.Iff, [left_side, right_side])
                    else:
                        print("Error: Illegal symbol " + connective + " between " + " ".join(tokens[1: right_location]) + " and " + " ".join(formula[right_location + 2:]))
                        return None
        else:
            print("Error: Illegal start of statement (" + current_token + "). See syntax rules.")
            return None


if __name__ == '__main__':
    atoms = []
    values = []
    current_user_task = Tasks.EnterAtoms
    while True:
        while current_user_task == Tasks.EnterAtoms:
            atoms = input("Enter a list of unique letters, separated by spaces. These will be the atomic statements.\n").lower().split()
            if len(atoms) == 0:
                print("Error: You must enter at least one letter.")
            elif atoms[0] == "quit" or atoms[0] == 'q':
                exit(0)
            elif not all(len(a) == 1 for a in atoms):
                print("Error: Multi-character strings are not accepted. You entered " + ', '.join(filter(lambda a: len(a) > 1, atoms)))
            elif not all(a.isalpha() for a in atoms):
                print("Error: Only letters are accepted. You entered " + ', '.join(filter(lambda a: not a.isalpha(), atoms)))
            elif len(set(atoms)) != len(atoms):
                print("Error: letters are not distinct. You have multiple instances of " + ', '.join(set(filter(lambda a: atoms.count(a) > 1, atoms))))
            else:
                current_user_task = Tasks.EnterFormula
        while current_user_task == Tasks.EnterFormula:
            statement = input("Enter a propositional statement. Enter 'help' to get the syntax for a propositional statement.\n").lower()
            if "help" in statement:
                print("""A propositional statement is one of the following:
    -An atomic statement (atom)
    -An n-ary logical connective that connects n statements (molecule)
Only the following connectives are allowed:\n"""
                + "\t{:<80}{:<40}\n".format("Not: denoted by \'not\' or the symbol [!].", "Syntax: !(statement)")
                + "\t{:<80}{:<40}\n".format("Or: denoted by \'or\' or the symbol [|].", "Syntax: (statement1) | (statement2)")
                + "\t{:<80}{:<40}\n".format("And: denoted by \'and\' or the symbol [&].", "Syntax: (statement1) & (statement2)")
                + "\t{:<80}{:<40}\n".format("Implies (aka If-Then): denoted by \'implies\' or the symbol [->].", "Syntax: (statement1) -> (statement2)")
                + "\t{:<80}{:<40}\n".format("If and Only If (aka Iff): denoted by \'iff\' or the symbol [<->].", "Syntax: (statement1) <-> (statement2)")
                + """If a statement is an atom, you do not need to put brackets around it, but otherwise you should.
If you don't, ungrouped operations will be conducted with right associativity.
                """)
            elif "quit" in statement:
                exit(0)
            else:
                tokens = generate_tokens(statement)
                if tokens is not None:
                    completed_formula = parse_formula(atoms, tokens)
                    if completed_formula is not None:
                        print_evaluation(completed_formula)
                        next_move_chosen = False
                        while not next_move_chosen:
                            next_move = input("Enter \'f\' to try another formula, \'r\' to give a new set of atoms, or \'q\' to quit.\n").lower()
                            if next_move == "f":
                                next_move_chosen = True
                            elif next_move == "r":
                                current_user_task = Tasks.EnterAtoms
                                next_move_chosen = True
                            elif next_move == "q":
                                exit(0)
                            else:
                                print("Invalid input.")